<?php

namespace TodoList\Controllers;

class MainController
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }
}