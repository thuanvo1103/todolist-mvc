'use strict';

$(function() {
    // Initialize date picker:
    $('.datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd-mm-yyyy',
        weekStart: 1
    });

    // Button events:
    $('.btn-work-edit').on('click', function() {
        let $tableRow = $(this).parents('.table-row');

        $tableRow.find('.cell-value, .cell-edit-delete').hide();
        $tableRow.find('.cell-input').show();
        $tableRow.find('.cell-save-discard').css('display', 'inline-flex');
    });

    $('.btn-work-discard').on('click', function() {
        let $tableRow = $(this).parents('.table-row');

        $tableRow.find('.cell-input, .cell-save-discard').hide();
        $tableRow.find('.cell-value').show();
        $tableRow.find('.cell-edit-delete').css('display', 'inline-flex');
    });

    $('.btn-work-delete').on('click', function() {
        if (confirm('Are you sure you want to DELETE this work?')) {
            window.location.href = $(this).data('href');
        }
    });

    $("#startingDateInput, #endingDateInput").change(function(){
        let start = $("#startingDateInput").val();
        let end = $("#endingDateInput").val();
        compareDateAdd(start, end);
    });

    $("#startDate_update, #endingDate_update").change(function(){
        let start = $("#startDate_update").val();
        let end = $("#endingDate_update").val();
        compareDateUpdate(start, end);
    });
    
    function compareDateAdd(start, end){
        if(new Date(start) > new Date(end))
        {
            alert("Start date is greater than the end date");
            $(".btn-submit-form").prop('disabled', true);
        }
        else{
            $(".btn-submit-form").prop('disabled', false);
        }
    }

    function compareDateUpdate(start, end){
        if(new Date(start) > new Date(end))
        {
            alert("Start date is greater than the end date");
            $(".btn-work-update").prop('disabled', true);
        }
        else{
            $(".btn-work-update").prop('disabled', false);
        }
    }
});