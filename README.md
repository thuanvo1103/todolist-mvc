#### This is a Todo List implemented in PHP and MySQL following MVC pattern. 

### Folow the instructions step by step to run the project

1. Run the following command from command line to install dependencies

    ```composer install```
    
2. Run the following command from command line to dump autoload
    
    ```composer dump-autoload```

2. Import database dumb from ```todo-list.sql```

3. Open ```localhost/todolist-php/``` to see the result.

4. To run unit tests, execute this from command line:
    
    ```./vendor/bin/phpunit```


